"""Find tasks with restart=true metadata and restart them."""
import logging
import os
import sys
from typing import Dict, List

import requests
from requests import codes as http


def setup_logger(name: str, level: int = logging.INFO) -> logging.Logger:
    """Set up a logger for stdout.

    Args:
    ----
        name: Name of the logger.
        level: Logging level.

    Returns:
    -------
        Configured Logger instance.
    """
    # Create a logger
    logger = logging.getLogger(name)
    # If the logger has handlers then it is already configured
    if logger.handlers:
        return logger
    logger.setLevel(level)

    # Create a handler for stdout
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(level)

    # Create a formatter and set it for the handler
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    handler.setFormatter(formatter)

    # Add the handler to the logger
    logger.addHandler(handler)

    return logger


def get_nomad_jobs(nomad_url: str) -> List[str]:
    """Get a list of job IDs from the Nomad API.

    Args:
    ----
        nomad_url: URL of the Nomad server.

    Returns:
    -------
        List of job IDs.
    """
    response = requests.get(f"{nomad_url}/v1/jobs", timeout=60)
    jobs = response.json()
    return [job["ID"] for job in jobs]


def get_job_detail(nomad_url: str, job_id: str) -> Dict:
    """Get details of a specific job.

    Args:
    ----
        nomad_url: URL of the Nomad server.
        job_id: ID of the job.

    Returns:
    -------
        Job detail as a dictionary.
    """
    response = requests.get(f"{nomad_url}/v1/job/{job_id}", timeout=60)
    return response.json()


def restart_tasks(job_details: Dict, nomad_url: str) -> None:
    """Extract metadata from task configurations in a job.

    Args:
    ----
        job_details: Job details containing task groups and tasks.
        nomad_url: The URL to use for restarting tasks via the api.

    Returns:
    -------
        Dictionary of task names and their metadata.
    """
    for group in job_details.get("TaskGroups", []):
        for task in group.get("Tasks", []):
            if (
                task.get("Meta", {}) is not None
                and task.get("Meta", {}).get("nightly_restart", "false") == "true"
            ):
                allocations = get_running_allocations(nomad_url, job_details["ID"])
                for allocation in allocations:
                    restart_task_in_allocation(nomad_url, allocation, task["Name"])


def restart_task_in_allocation(nomad_url: str, alloc_id: str, task_name: str) -> None:
    """Restart a specific task within an allocation in Nomad.

    Args:
    ----
        nomad_url: URL of the Nomad server.
        alloc_id: The UUID of the allocation.
        task_name: The name of the task to restart.

    Raises:
    ------
        Exception: If the request to the Nomad API fails.
    """
    endpoint = f"{nomad_url}/v1/client/allocation/{alloc_id}/restart"
    payload = {"TaskName": task_name}
    logger = setup_logger("the-restarter")

    logger.info("Task '%s' in allocation '%s' will be restarted.", task_name, alloc_id)
    response = requests.post(endpoint, json=payload, timeout=60)

    if response.status_code != http.OK:
        errmsg = f"Failed to restart task: {response.text}"
        raise Exception(errmsg)

    logger.info("Task '%s' in allocation '%s' has been restarted.", task_name, alloc_id)


def get_running_allocations(nomad_url: str, job_id: str) -> list:
    """Get all running allocation IDs for a given job in Nomad.

    Args:
    ----
        nomad_url: URL of the Nomad server.
        job_id: The ID of the job.

    Returns:
    -------
        A list of allocation IDs that are currently running for the given job.

    Raises:
    ------
        Exception: If the request to the Nomad API fails.
    """
    endpoint = f"{nomad_url}/v1/job/{job_id}/allocations"
    response = requests.get(endpoint, timeout=60)

    if response.status_code != http.OK:
        msg = f"Failed to get allocations: {response.text}"
        raise Exception(msg)

    allocations = response.json()
    return [alloc["ID"] for alloc in allocations if alloc["ClientStatus"] == "running"]


def main() -> None:
    """Start the process."""
    if "NOMAD_URL" not in os.environ:
        logging.fatal("No NOMAD_URL specified")
        sys.exit(1)
    nomad_url = os.environ["NOMAD_URL"]

    for job_id in get_nomad_jobs(nomad_url):
        job_details = get_job_detail(nomad_url, job_id)
        restart_tasks(job_details, nomad_url)


if __name__ == "__main__":
    main()
