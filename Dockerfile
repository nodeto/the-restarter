ARG PYTHON_VERSION=3.12
FROM python:${PYTHON_VERSION} as deps

RUN useradd -m build
USER build

WORKDIR /home/build

ENV PATH=/home/build/.local/bin:$PATH
RUN pip install --no-cache --user --upgrade pip && \
    pip install --no-cache --user pipx && pipx install poetry==1.7.1

RUN mkdir -p /home/build/project
WORKDIR /home/build/project

COPY pyproject.toml poetry.lock ./
RUN python -m venv .venv \
    && . .venv/bin/activate \
    && poetry install --only=main --no-root

###
# DEPENDENCIES
###
FROM deps as build
COPY . .
RUN . .venv/bin/activate \
    && poetry build \
    --format=wheel \
    --no-interaction \
    && pip install --no-cache dist/*

# copy any scripts to /usr/local/bin with fixed shebangs
USER root
RUN mkdir /tmp/venv && \
    cp -r /home/build/project/.venv/. /tmp/venv/ \
    && find /tmp/venv \
    -executable \
    -print0 \
    | xargs -0 file \
    | grep 'text executable' \
    | cut -d: -f1 \
    | xargs -I {} \
    sed -i 's|#!\/home\/build\/project\/\.venv\/bin\/python|#!\/usr\/bin\/env python|' {} \
    && cp -nr /tmp/venv/. /usr/local/

###
# TEST
###
FROM deps AS test

COPY . .
RUN poetry install
RUN poetry run ruff . && poetry run ruff format --check .
RUN touch .tested

###
# FINAL
###
FROM python:${PYTHON_VERSION} as final

### SET USER AND ID HERE
ARG USER=the-restarter
ARG ID=261326133
### SHOULD CHANGE THIS FOR NEW PROJECTS ^^^

COPY --from=build /usr/local/. /usr/local/

RUN groupadd -g ${ID} ${USER} && \
    useradd -u ${ID} -g ${ID} ${USER}

USER ${USER}

ENTRYPOINT ["the-restarter"]

# to force 'build' of the tests
COPY --from=test /home/build/project/.tested /dev/null
